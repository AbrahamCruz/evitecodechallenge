package com.example.giphyapi.network

import android.content.Context
import android.util.Log
import com.example.giphyapi.R
import com.example.giphyapi.models.Giphy
import com.example.giphyapi.models.GiphyGifData
import com.example.giphyapi.presenters.DataValidation
import com.example.giphyapi.utils.NetworkUtils
import okhttp3.*
import org.json.JSONException
import java.io.IOException

//URL Example Format: http://api.giphy.com/v1/gifs/search?q=something&api_key=mykey=1&offset=0
class GiphyRequest(private val context: Context, private val search: String) {
    private lateinit var giphyGifData: Array<GiphyGifData>
    private lateinit var giphy: Giphy
    private var giphyRespnose = GiphyRespnose()

    private fun buildRequestURL(search: String): String {
        return BASE_URL + "?q=" + DataValidation.validateStringQuery(search, DEFAULT_TEXT) +
                "&api_key=" + context.getString(R.string.APIKey) +
                "&limit=1"
    }


    fun requestGiphyData() {

        if (NetworkUtils.isNetworkAvailable(context)) {

            val client = OkHttpClient()
            val request = Request.Builder()
                    .url(buildRequestURL(search))
                    .build()

            val call = client.newCall(request)
            call.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e(TAG, "error: " + e.message)
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    try {
                        val jsonData = response.body()!!.string()
                        if (response.isSuccessful) {
                            giphy = giphyRespnose.parseTrendingData(jsonData)
                            giphyGifData = giphyRespnose.getGif(jsonData)
                            Log.v(TAG, "Giphy Gif Data from Response: " + giphyGifData!!)
                        } else {
                            Log.i(TAG, "Response Unsuccessful")
                        }
                    } catch (e: IOException) {
                        Log.e(TAG, "Exception Caught: ", e)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            })
        } else {
            Log.e(TAG, "Network is not available!")
        }

    }

    companion object {

        private val TAG = GiphyRequest::class.java.simpleName
        private val BASE_URL = "http://api.giphy.com/v1/gifs/search"
        private val DEFAULT_TEXT = "how dare you"
    }

}
