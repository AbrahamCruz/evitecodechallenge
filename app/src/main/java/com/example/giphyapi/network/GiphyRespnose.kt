package com.example.giphyapi.network

import com.example.giphyapi.models.Giphy
import com.example.giphyapi.models.GiphyGifData

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class GiphyRespnose {

    @Throws(JSONException::class)
    fun getGif(jsonData: String): Array<GiphyGifData> {
        val giphy = JSONObject(jsonData)
        val data = giphy.getJSONArray("data")

        val NUM_OF_GIFS = data.length()
        val gifs = Array(1){GiphyGifData()}

        for (i in 0 until NUM_OF_GIFS) {
            val trendingGif = data.getJSONObject(i)
            val gif = GiphyGifData()

            val images = trendingGif.getJSONObject("images")
            val original = images.getJSONObject("original")

            gif.url = original.getString("url")

            gifs[i] = gif
        }

        return gifs
    }

    @Throws(JSONException::class)
    fun parseTrendingData(jsonData: String): Giphy {
        val giphy = Giphy(getGif(jsonData))

        giphy.setGiphyData(getGif(jsonData))

        return giphy
    }
}
