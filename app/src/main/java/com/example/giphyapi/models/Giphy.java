package com.example.giphyapi.models;

public class Giphy {

    private static GiphyGifData[] giphySearchData;

    public Giphy(GiphyGifData[] giphySearchData) {
        this.giphySearchData = giphySearchData;
    }

    public static GiphyGifData[] getGiphyData() {
        return giphySearchData;
    }

    public void setGiphyData(GiphyGifData[] giphyTrendingData) {
        this.giphySearchData = giphyTrendingData;
    }
}
