package com.example.giphyapi.presenters

object DataValidation {

    fun validateStringQuery(text: String, defaultText: String): String {
        return if (text != null && !text.isEmpty()) {
            text
        } else {
            defaultText
        }
    }
}
