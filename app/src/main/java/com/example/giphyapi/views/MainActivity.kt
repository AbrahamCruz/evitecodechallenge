package com.example.giphyapi.views

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget
import com.example.giphyapi.R
import com.example.giphyapi.models.Giphy
import com.example.giphyapi.network.GiphyRequest
import com.example.giphyapi.presenters.DataValidation

class MainActivity : AppCompatActivity() {

    private var searchText: EditText? = null
    private var giphyRequest: GiphyRequest? = null
    private var imageView: ImageView? = null
    private var refresh_button: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageView = findViewById(R.id.glide_gif)
        refresh_button = findViewById(R.id.refresh_button)
        searchText = findViewById(R.id.edittext)
        refresh_button!!.setOnClickListener { refresh() }
    }

    fun refresh() {
        getGiphy()
    }

    fun updateDisplay() {
        val gifs = Giphy.getGiphyData()
        if (gifs != null && gifs.size > 0) {
            val randImage = (Math.random() * gifs.size).toInt()
            val gif = gifs[randImage]
            val gifUrl = gif.url
            Log.d(TAG, "from update URL: $gifUrl")

            val into: Any = Glide.with(this@MainActivity)
                    .load(gifUrl)
                    .thumbnail(0.1f)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .into(object : GlideDrawableImageViewTarget(imageView!!) {
                        override fun onResourceReady(resource: GlideDrawable, animation: GlideAnimation<in GlideDrawable>?) {
                            super.onResourceReady(resource, animation)
                        }
                    })
        } else {
            refresh()
        }
    }

    fun getGiphy() {
        giphyRequest = GiphyRequest(this, DataValidation.validateStringQuery(searchText!!.text.toString(), getString(R.string.DEFAULT_TEXT)))
        giphyRequest!!.requestGiphyData()
        updateValuesOnUI()
    }

    fun updateValuesOnUI() {
        val mainHandler = Handler(this.mainLooper)
        val myRunnable = Runnable { updateDisplay() }
        mainHandler.post(myRunnable)
    }

    companion object {
        val TAG = MainActivity::class.java!!.getSimpleName()
    }

}