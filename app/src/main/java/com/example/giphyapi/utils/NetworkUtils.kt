package com.example.giphyapi.utils

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager

class NetworkUtils {
    companion object {

        fun isNetworkAvailable(context: Context): Boolean {
            val manager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = manager.activeNetworkInfo
            var isAvailable = false
            if (networkInfo != null && networkInfo.isConnected) {
                isAvailable = true
            }
            return isAvailable
        }
    }
}
